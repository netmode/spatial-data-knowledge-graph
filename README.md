# Spatial-data-knowledge-graph

This project provides a set of mechanisms in order to achieve the proper representation and ingestion of spatial data in a LPG Knowledge Graph, as well as support the visualisation and analysis of the spatial data retrieved by a KG. The data management functions are provided over the SustainGraph and the data used regard climate variables in Athens.

## Prerequisites

Populate the [SustainGraph](https://gitlab.com/netmode/sustaingraph/-/wikis/home), a Knowledge Graph which tracks information related to the evolution of Sustainable Development Goal (SDG) indicators.

For any additional libraries used in the jupyter notebooks, import them in the jupyter notebook directly using the corresponding pip command.

e.g. for the geopandas library create a new cell containing the following command and then run it:

```
!pip install geopandas
```

Additional instructions are provided within each jupyter notebook in order to download the necessary data files.

## Description

- _Climate_Spatial_Data.ipynb_ contains the process followed for cleaning, preprocessing, and importing the spatial data in the SustainGraph. It also contains some methods for reducing the size of the data using spatial aggregation. 

- _Climate_Spatial_Data_Analysis.ipynb_ contains an analysis over the climate spatial data and other indicators with spatial characteristics.

